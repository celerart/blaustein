<?php
$postID = $post->ID;
$postLink = get_permalink($postID);
?>
<div class="col-lg-4">
  <div class="service-item">
    <div class="icon">
      <img src="<?php echo carbon_get_post_meta($postID, 'post_icon'); ?>" alt="">
    </div>
    <div class="si-title">
      <a href="<?php echo $postLink; ?>" class="si-link"><?php the_title(); ?></a>
    </div>
    <div class="si-desc">
      <p>
        <?php echo cutPostIntro($post->post_content, 120); ?>
      </p>
    </div>
    <div class="read-more">
      <a href="<?php echo $postLink; ?>">
        <span class="label">Learn More</span>
      </a>
    </div>
  </div>
</div>