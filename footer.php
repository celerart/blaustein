<?php
/**
 * The template for displaying the footer
 *
 */

$templatePath = get_template_directory_uri();
?>
</div>
<footer class="main-footer">

</footer>
</div>
<?php wp_footer(); ?>
<script src="<?php echo $templatePath; ?>/js/bootstrap.min.js""></script>
<script src="<?php echo $templatePath; ?>/js/script.min.js"></script>
</body>
</html>
