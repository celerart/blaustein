<?php
/**
 * Template Name: Main
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage blendNew
 * @since BlendNew 1.0
 */

$postID = $post->ID;
$templatePath = get_template_directory_uri();

get_header(); ?>
<div class="main-banner">
  <div class="container">
    <div class="banner-content">
      <div class="row">
        <div class="col-lg-8">
          <div class="banner-content_inner">
            <div class="banner-title"><?php echo carbon_get_post_meta($postID, 'hp_banner_title'); ?></div>
            <div class="banner-text"><?php echo wpautop(carbon_get_post_meta($postID, 'hp_banner_text')); ?></div>
            <div class="banner-form">
              <div class="site-form">
                <div class="form-item">
                  <input type="email" class="input-border required" name="email" placeholder="Enter Your Email Address">
                  <span class="error-mess">Error message</span>
                </div>
                <div class="buttons">
                  <button class="button send-email-form">Get in Touch</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button class="anchor-button" data-target="#anchor">
        <img src="<?php echo $templatePath; ?>/img/arrow-down.svg" alt="">
      </button>
    </div>
  </div>
</div>
<?php get_template_part('components/section', 'services'); ?>
<?php get_footer(); ?>

