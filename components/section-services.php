<?php
  $serviceSubtitle = 'Agency Services';
  if (is_front_page()) {
    $serviceSubtitle = 'What We Do';
  }
  $args = array(
    'posts_per_page' => -1,
    'post_type' => 'post',
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'field'    => 'slug',
        'terms'    => array('services'),
        'operator' => 'IN',
      )
    )
  );
  $query = new WP_Query( $args );
?>
<div class="services-section section" id="anchor">
  <div class="container">
    <div class="service-subtitle"><?php echo $serviceSubtitle; ?></div>
    <div class="section-title">Jumpstart your online<br>presence</div>
    <?php
      if ( $query->have_posts() ) {
        echo '<div class="row">';
        while ($query->have_posts()) {
          $query->the_post();
          get_template_part('content', 'service');
        }
        echo '</div>';
        wp_reset_postdata();
      }
    ?>
  </div>
</div>