<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
if ( have_posts() ) {
  if (!empty($_GET['s'])) {
    $title = 'Search results for: '.get_search_query();
    $bannerText = '';
  } else {
    $title = 'Empty request';
    $bannerText = '';
  }
} else {
  $title = 'Nothing Found';
  $bannerText = '<div class="banner-text"><p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p></div>';
}
?>
<div class="inner-page search">
  <div class="main-banner" style="background-image: url(<?php echo carbon_get_term_meta(3, 'product_image'); ?>);">
    <div class="main-banner_inner">
      <div class="banner-inner">
        <h1 class="banner-title"><?php echo $title; ?></h1>
        <?php echo $bannerText; ?>
        <?php echo get_search_form(); ?>
      </div>
    </div>
  </div>
  <div class="news big-first">
    <div class="container-max">
      <?php if ( have_posts() ) {
        if (!empty($_GET['s'])) {
          get_template_part('components/news');
        }
      } ?>
    </div>
  </div>
</div>

<?php
get_footer();
