<?php
/**
 * Celerart function
 *
 * @package WordPress
 * @since celerart 1.0
 */
add_filter('show_admin_bar', '__return_false');

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since celerart 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 900;
}
/**
 *Add woocommerce support.
 */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
/**
* Make theme available for translation.
*/

load_theme_textdomain( 'celerart' );

/**
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

/**
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array(
	'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
) );

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 */

function celerart_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'celerart_javascript_detection', 0 );

/**
 * This theme uses wp_nav_menu() in header locations.
 */
register_nav_menus( array(
  'primary' => __( 'Header',      'celerart' )
) );

/**
 * Register widget area.
 *
 */
function celerart_widgets_init() {
  register_sidebar( array(
      'name'          => __( 'Shop filters', 'celerart' ),
      'id'            => 'celerart-filters',
      'description'   => __( 'Shop filters', 'celerart' ),
      'before_widget' => '',
      'after_widget'  => '',
      'before_title'  => '',
      'after_title'   => '',
  ) );
}
//add_action( 'widgets_init', 'celerart_widgets_init' );


/**
 * Add style and script
 *
 */

function celerart_scripts_method() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '/wp-content/themes/blaustein/js/jquery.min.js');
	wp_enqueue_script( 'jquery' );
}

function boot()
{
	wp_enqueue_style('main_stylesheet', get_stylesheet_uri());
	wp_enqueue_style('main-style', get_template_directory_uri() . "/css/style.min.css", rand(111, 9999), array('main_stylesheet'));
	wp_enqueue_style('fonts', get_template_directory_uri() . "/css/fonts.css");
	//wp_enqueue_style('animate-css', get_template_directory_uri() . "/css/animate-css/animate.min.css");
//	wp_enqueue_style('slick', get_template_directory_uri() . "/css/slickSlider/slick.css");
//	wp_enqueue_style('countDown', get_template_directory_uri() . "/css/countDown/styles.css");
//	wp_enqueue_style('bootstrap', get_template_directory_uri() . "/css/bootstrap/bootstrap.min.css");
	wp_enqueue_style('bootstrap-grid', get_template_directory_uri() . "/css/bootstrap.min.css");
//  wp_enqueue_style('fancybox', get_template_directory_uri() . "/css/jquery.fancybox.min.css");
//	wp_enqueue_style('slick-theme', get_template_directory_uri() . "/css/slickSlider/slick-theme.css");
//	wp_enqueue_style('animate', get_template_directory_uri() . "/css/animate_css/animate.css");


//	wp_enqueue_script('popper',  "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", '','', true);
//	wp_enqueue_script('custom-js', get_template_directory_uri() . "/build/script/js/main.js", array('bootstrap'),'', true);
//	wp_enqueue_script('slick-js', get_template_directory_uri() . "/js/slickSlider/slick.min.js", '', true);
//	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . "/js/bootstrap/bootstrap.min.js", '', true);
//  wp_enqueue_script('fancybox', get_template_directory_uri() . "/js/jquery.fancybox.min.js", array(),'', true);
//	wp_enqueue_script('wow-js', get_template_directory_uri() . "/js/wow-js/wow.min.js", '', true);
//  wp_enqueue_script('custom-js', get_template_directory_uri() . "/js/script.min.js", '', true);
}
add_action('wp_enqueue_scripts', 'boot', 12);
add_action( 'wp_enqueue_scripts', 'celerart_scripts_method', 11 );



function cutPostIntro($intro, $length) {
  $postIntro = $intro;
  $introLength = mb_strlen($postIntro);
  if ($introLength > $length) {
    $cutPostIntro = mb_substr($postIntro, 0, $length).'...';
  } else {
    $cutPostIntro = $postIntro;
  }
  return $cutPostIntro;
}

/**
 * Carbon fields active if need
 *
 * @since Carbon fields 3.0
 */
require 'carbon-fields/carbon-fields-plugin.php';

/**
 * Carbon fields add to main load
 *
 */

function crb_load() {
  require_once( 'carbon-fields/vendor/autoload.php' );
  \Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'after_setup_theme', 'crb_load' );

/**
 * Carbon fields metafield
 *
 */
use Carbon_Fields\Container;
use Carbon_Fields\Field;

function crb_attach_theme_options() {
  require 'inc/meta-fields.php';
}

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );

//disable gutenberg
add_filter("use_block_editor_for_post_type", "disable_gutenberg_editor");
function disable_gutenberg_editor()
{
  return false;
}

//dashicons support
function ww_load_dashicons(){
  wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'ww_load_dashicons');