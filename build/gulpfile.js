'use strict';
// Include Packages
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');


// npm install gulp-sass gulp-autoprefixer gulp-clean-css gulp-rename gulp-sourcemaps gulp-concat gulp-uglify

// Convert sass file to css123
gulp.task('sass', function () {
    return gulp.src('style/**/*.sass')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: [
                "last 15 version",
                "> 1%",
                "IE 10"
            ],
        }))
        .pipe(cleanCSS({format: 'keep-breaks'}))
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../css'));
});

// Compress js
gulp.task('compress-js',function () {
    return gulp.src('script/js/main.js')
    // .pipe(concat('scripts.min.js'))
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'));
});

//Watch
gulp.task('watch', function(){
    gulp.watch('style/**/*.sass', gulp.series('sass'));
    gulp.watch('script/js/*.js', gulp.series('compress-js'));
});

//Default
gulp.task('default', gulp.series('watch'));