<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();?>
<div class="inner-page error-page">
  <div class="container-my">
    <div class="section">
      <div class="section-heading">404</div>
      <div class="image">
        <img src="<?php echo get_template_directory_uri(); ?>/img/404-min.png" alt="">
      </div>
      <div class="message">
        הדף לא נמצא, חזור <a href="<?php echo home_url(); ?>">לדף הראשי</a>
      </div>
    </div>
  </div>
</div>
<?php get_footer();?>
