<?php
  $postID = $post->ID;
  $link = get_permalink($postID);
  $monthArr = array(1 => 'ינואר', 2 => 'פברואר', 3 => 'מרץ', 4 => 'אפריל', 5 => 'מאי', 6 => 'יוני', 7 => 'יולי', 8 => 'אוגוסט', 9 => 'ספטמבר', 10 => 'אוקטובר', 11 => 'נובמבר', 12 => 'דצמבר');
  $postDate = strtotime($post->post_date);
  $day = date('d', $postDate);
  $month = date('n', $postDate);
  $year = date('Y', $postDate);
  $dateMod = $day.' '.$monthArr[$month].' '.$year;
?>
<div class="post-item">
  <div class="post-item_inner">
    <div class="post-item_image">
      <a href="<?php echo $link; ?>">
        <img src="<?php echo get_the_post_thumbnail_url($postID, 'large'); ?>" alt="">
      </a>
    </div>
    <div class="post-item_content">
      <div class="pi-content_top">
        <div class="post-title">
          <a href="<?php echo $link; ?>"><?php the_title(); ?></a>
        </div>
        <div class="post-date"><?php echo $dateMod; ?></div>
      </div>
      <div class="post-excerpt">
        <p><?php echo cutPostIntro($post->post_content, 150); ?></p>
      </div>
      <div class="post-detail">
        <a href="<?php echo $link; ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-long-yellow.svg" alt="">
        </a>
      </div>
    </div>
  </div>
</div>